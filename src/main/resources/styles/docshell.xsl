<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:x="docshell.cloud/schema/docshell" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<head>
				<link rel="stylesheet" type="text/css" href="https://www.docshell.cloud/xsl/docshell.css" />
				<script src="https://www.docshell.cloud/xsl/docshell.js" />
				<script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.9.0/showdown.min.js" />
				<script src="https://unpkg.com/mermaid@7.1.0/dist/mermaid.js" />
				<script>mermaid.initialize({startOnLoad:true});</script>
			</head>
			<body onload="init()">
			
			    
				<xsl:for-each select="x:docshell">
					<div class="configuration">
						<div class="title">
							⚙ 
							<xsl:value-of select="x:title" />
							<xsl:if test="x:version">
								- version: 
								<xsl:value-of select="x:version" />
							</xsl:if>
						</div>
						
						<xsl:if test="x:author">
							<div class="author">
								👨‍💻 
								<xsl:value-of select="x:author" />
							</div>
						</xsl:if>
						<xsl:if test="x:os/x:family">
							<div class="author">
								🧩
								<xsl:value-of select="x:os/x:family"  />
								<xsl:if test="x:os/x:distribution">
									&#8594; <xsl:value-of select="x:os/x:distribution" />
									<xsl:if test="x:os/x:version">
										&#8594; <xsl:value-of select="x:os/x:version" />
									</xsl:if>
								</xsl:if>
							</div>
						</xsl:if>
							<form class="author" id="param-list-form">
						    </form>
						<xsl:for-each select="x:param">
					    	<script>
					    	    p = {name:'<xsl:value-of select="x:name" />',description:'<xsl:value-of select="x:description" />',defaultValue:'<xsl:value-of select="x:defaultValue" />'};
					    	    params.push(p);
					    	</script>
						</xsl:for-each>
						<xsl:if test="x:tag">
							<div class="author">
								
								<xsl:for-each select="x:tag">
									🗂️ <xsl:value-of select="." />
								</xsl:for-each>
							</div>
						</xsl:if>
						<xsl:if test="x:dependency">
							<div class="author">
								<xsl:for-each select="x:dependency">
									⛓️ <a href="{x:uri}"><xsl:value-of select="x:uri" /></a> 
								</xsl:for-each>
							</div>
						</xsl:if>
						<xsl:if test="x:comment">
							<div class="comment md">
								<xsl:value-of select="x:comment" disable-output-escaping="yes" />
							</div>
						</xsl:if>
						<xsl:for-each select="x:command">
							<div class="command">
								<xsl:if test="x:comment">
									<div class="comment">
										<xsl:value-of select="x:comment" disable-output-escaping="yes" />:
									</div>
								</xsl:if>
								<div class="io" id="cmd{x:id}">
									<a class="command-a" href="#cmd{x:id}">🔗 </a>
									<xsl:for-each select="x:io">
										<xsl:if test="x:comment">
											<div class="comment">
												&gt;&gt; <xsl:value-of select="x:comment" />:
											</div>
										</xsl:if>
						
										<xsl:if test="@type='input' and position() = 1">
											<xsl:apply-templates select="../x:shellType" />
										</xsl:if>
										<xsl:if test="@type='input'">
											<span class="input" ondblclick="selectAllText(this,'cmd{../x:id}')">
												<xsl:value-of select="x:value" />
											</span>
												<br/>
										</xsl:if>
										<xsl:if test="@type='output'">
											<span class="output">
												<xsl:value-of select="x:value" />
											</span>
										</xsl:if>
										<xsl:if test="@type='base64'">
											<div class="filecontent-container">
												<textarea readonly="true" class="filecontent b64" onclick="selectContent(event);">
													<xsl:value-of select="x:value" />
												</textarea>
												<div>clicca per copiare</div>
											</div>
										</xsl:if>
										<xsl:if test="@type='diagram'">
											<div class="mermaid">
												<xsl:value-of select="x:value" />
											</div>
										</xsl:if>
										<xsl:if test="@type='filecontent'">
											<div class="filecontent-container">
												<textarea readonly="true" class="filecontent" onclick="selectContent(event);">
													<xsl:value-of select="x:value" />
												</textarea>
												<div>clicca per copiare</div>
											</div>
										</xsl:if>
									</xsl:for-each>
								</div>
							</div>
						</xsl:for-each>
					</div>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>


<xsl:template match="x:shellType[text() = 'POSTGRESQL']">
	<span class="prompt"><span class="server db"><xsl:value-of select="../x:property[@key='database']" /></span><xsl:value-of select="../x:property[@key='prompt']" /> </span>
</xsl:template>

<xsl:template match="x:shellType[text() = 'MYSQL']">
	<span class="prompt"><span class="server db">mysql</span>&gt; </span>
</xsl:template>

<xsl:template match="x:shellType[text() = 'BASH']">
	<xsl:choose>
		<xsl:when test="(/x:server/x:name = ../x:property[@key='serverName']) or not(../x:property[@key='serverName']) or (../x:property[@key='serverName'] = '')" >
		<span class="prompt"><span class="server bash"><xsl:value-of select="../x:property[@key='user']" />@<xsl:value-of select="/x:server/x:name" /></span>:<xsl:value-of select="../x:property[@key='wd']" />$ </span>
		</xsl:when>
		<xsl:otherwise>
		<span class="prompt"><span class="server bash other"><xsl:value-of select="../x:property[@key='user']" />@<xsl:value-of select="../x:property[@key='serverName']" /></span>:<xsl:value-of select="../x:property[@key='wd']" />$ </span>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
</xsl:stylesheet>