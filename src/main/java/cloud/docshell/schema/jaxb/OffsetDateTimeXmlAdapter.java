package cloud.docshell.schema.jaxb;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class OffsetDateTimeXmlAdapter extends XmlAdapter<String, OffsetDateTime> {

	@Override
	public OffsetDateTime unmarshal(final String stringValue) {
		return stringValue != null
				? DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(stringValue, OffsetDateTime::from)
				: null;
	}

	@Override
	public String marshal(final OffsetDateTime value) {
		return value != null
				? DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(value)
				: null;
	}
}