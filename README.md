# docshell-schema

Xml schema for docshell data

## docshell

`docshell.xsd` and classes `cloud.docshell.schema.docshell.*` are stable from v. 1.0.0

## docshell-server

`docshell-server.xsd` and classes `cloud.docshell.schema.server.*` are work in progress
I have not yet decided the final structure of the project